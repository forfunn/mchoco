//
//  UITableViewCell+MC.swift
//  MChoco
//
//  Created by IT on 31/01/2019.
//  Copyright © 2019 None. All rights reserved.
//

import Foundation

// MARK: - UITableViewCell
extension UITableViewCell {
    
    /// 注册获取nib
    ///
    /// - Returns: 对应的nib
    public class func nib() -> UINib {
        return UINib(for: self)
    }
    
    /// identifier
    ///
    /// - Returns: 类名
    public class func identifier() -> String {
        return NSStringFromClass(self)
    }
}
