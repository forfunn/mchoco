//
//  DateFormatStyle.swift
//  MChoco
//
//  Created by Richie on 2019/1/6.
//  Copyright © 2019 person. All rights reserved.
//

import Foundation

/// 日期时间格式
public struct DateFormatStyle: RawRepresentable, Hashable, Equatable {
    //    MARK: - RawRepresentable
    public init(rawValue: String) {
        self.rawValue = rawValue
    }
    
    public var rawValue: String
    
    public typealias RawValue = String
    
    //    MARK: - Hashable
    public var hashValue: Int {
        get {
            return self.rawValue.hashValue
        }
    }

    //    MARK: - Equatable
    public static func == (lhs: DateFormatStyle, rhs: DateFormatStyle) -> Bool {
        return lhs.rawValue == rhs.rawValue
    }
}

// MARK: - 常量扩展
extension DateFormatStyle {
    
    /// 日期
    public static let date = DateFormatStyle(rawValue: "yyyy-MM-dd")
    
    /// 日期时间
    public static let datetime = DateFormatStyle(rawValue: "yyyy-MM-dd HH:mm:ss")
}
