//
//  UITableViewHeaderFooterView+MC.swift
//  MChoco
//
//  Created by IT on 31/01/2019.
//  Copyright © 2019 None. All rights reserved.
//

import Foundation


/// header footer biaoshi
///
/// - header: header
/// - footer: footer
public enum MCHeaderFooter: String {
    case header = "header"
    case footer = "footer"
}


// MARK: - UITableViewHeaderFooterView
extension UITableViewHeaderFooterView {
    
    /// 注册获取nib
    ///
    /// - Returns: 对应的nib
    public class func nib() -> UINib {
        return UINib(for: self)
    }
    
    
    /// identifier
    ///
    /// - Returns: 类名
    public class func identifier() -> String {
        return NSStringFromClass(self)
    }
    
    
    /// 重用id
    ///
    /// - Parameter kind: 类型 header or footer
    /// - Returns: 字符串 classk+kind
    public class func identifier(for kind: MCHeaderFooter) -> String {
        return self.identifier().appending(kind.rawValue)
    }
}
