//
//  Operation.swift
//  MChoco
//
//  Created by IT on 09/01/2019.
//  Copyright © 2019 Person. All rights reserved.
//

import Foundation

/// Operation
public protocol Operation { }

// MARK: - Any: Operation
extension Operation where Self: Any {
    
    /// 进行操作
    ///
    /// - Parameter function: 方法
    /// - Throws: 错误
    public func operation(with function: (Self) throws -> Void) rethrows -> Void {
        try function(self)
    }
    
    /// 值拷贝
    ///
    /// - Parameter function: 进行的方法
    /// - Returns: self
    /// - Throws: 错误
    public func reconfiguration(with function: (inout Self) throws -> Void) rethrows -> Self {
        var copy = self
        try function(&copy)
        return copy
    }
}


// MARK: - AnyObject: Operation
extension Operation where Self: AnyObject {
    
    /// 进行配置
    ///
    /// - Parameter function: 配置
    /// - Returns: self
    /// - Throws: 错误
    public func configuration(with function: (Self) throws -> Void) rethrows -> Self {
        try function(self)
        return self
    }
}

// MARK: - NSObject: Operation
extension NSObject: Operation {}

// MARK: - String: Operation
extension String: Operation {}


import CoreGraphics

// MARK: - CGPoint : Operation
extension CGPoint: Operation {}

// MARK: - CGRect : Operation
extension CGRect: Operation {}

// MARK: - CGSize : Operation
extension CGSize: Operation {}

// MARK: - CGVector : Operation
extension CGVector: Operation {}



#if os(iOS) || os(tvOS)

import UIKit.UIGeometry

// MARK: - UIEdgeInsets: Operation
extension UIEdgeInsets: Operation {}

// MARK: - UIOffset: Operation
extension UIOffset: Operation {}

// MARK: - UIRectEdge: Operation
extension UIRectEdge: Operation {}
#endif
