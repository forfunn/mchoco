//
//  UINib+MC.swift
//  MChoco
//
//  Created by IT on 29/01/2019.
//  Copyright © 2019 None. All rights reserved.
//

import UIKit

// MARK: - UINib
extension UINib {
    
    /// 根据类型名称找到nib
    ///
    /// - Parameter aClass: AnyClass
    convenience init(for aClass: AnyClass) {
        self.init(nibName: NSStringFromClass(aClass), bundle: Bundle(for: aClass))
    }
}
