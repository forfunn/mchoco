//
//  String+MC.swift
//  MChoco
//
//  Created by Richie on 2019/1/4.
//  Copyright © 2019 person. All rights reserved.
//

import Foundation

// MARK: - String Extension
extension String {
    
    /// 图片链接编码 对图片中有特殊字符进行编码
    ///
    /// - Returns: 图片链接转码
    public func urlQueryEncoding() -> String? {
        return self.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
    }
    
    /// 对已有格式的字符串格式化
    ///
    /// - Parameters:
    ///   - characterSet: 包含的字符
    ///   - offset: 偏移
    /// - Returns: 格式化后的字符串
    public func reformat(trimming character: String, offsetby offset: Int) -> String {
        var variable = self
        var contents = [String]()
        if variable.count > 0 {
            let characterSet = CharacterSet(charactersIn: character)
            if let _ = variable.rangeOfCharacter(from: characterSet) {
                variable = variable.trimmingCharacters(in: characterSet)
            }
            while variable.count > 0 {
                if let start = variable.index(variable.startIndex, offsetBy: offset, limitedBy: variable.endIndex) {
                    contents.append(String(variable[..<start]))
                    variable = String(variable[start...])
                } else {
                    contents.append(variable)
                    variable = ""
                }
            }
            return contents.joined(separator: character)
        } else {
            return self
        }
    }

    /// 将数据转换成金额的格式
    ///
    /// - Parameters:
    ///   - money: 金钱 NSNumber
    ///   - style: 类型
    /// - Returns: 如果转换失败 为类型的默认 或者返回类型
    public static func reformat(with money: NSNumber, style: MoneyStyle) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.positiveFormat = style.positiveFormat
        return numberFormatter.string(from: money) ?? style.empty
    }
    
    /// 转换成NSNumber
    ///
    /// - Parameter style: 格式
    /// - Returns: 返回对应的Number
    public func money(from style: MoneyStyle) -> NSNumber? {
        let numberFormatter = NumberFormatter()
        numberFormatter.positiveFormat = style.positiveFormat
        return numberFormatter.number(from: self)
    }
    
    
}
