//
//  Array+JSON.swift
//  MChoco
//
//  Created by Richie on 2019/1/4.
//  Copyright © 2019 person. All rights reserved.
//

import Foundation

// MARK: - Array extension
extension Array {
    
    /// 将数组转换成 json字符串
    ///
    /// - Returns: 如果成功转换成字符串 否则返回 nil
    /// - Throws: error
    public func jsonString() throws -> String? {
        let data = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
        return String(data: data, encoding: .utf8)
    }
    
}
