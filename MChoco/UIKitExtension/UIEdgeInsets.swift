//
//  UIEdgeInsets.swift
//  MChoco
//
//  Created by IT on 11/01/2019.
//  Copyright © 2019 None. All rights reserved.
//

import Foundation


// MARK: - UIEdgeInsets
public extension UIEdgeInsets {
    
    /// 上下左右全部相同
    ///
    /// - Parameter all: 全部相同的数值
    init(with all: CGFloat) {
        self.init(top: all, left: all, bottom: all, right: all)
    }
    
    /// 水平 垂直
    ///
    /// - Parameters:
    ///   - horizontal: 水平 left right
    ///   - vertical: 垂直 top bottom
    init(with horizontal: CGFloat = 0, vertical: CGFloat = 0) {
        self.init(top: vertical, left: horizontal, bottom: vertical, right: horizontal)
    }
}
