//
//  UIViewController+MC.swift
//  MChoco
//
//  Created by IT on 12/01/2019.
//  Copyright © 2019 None. All rights reserved.
//

import Foundation

// MARK: - UIViewController Extension
extension UIViewController {
    
    /// 弹出 (present(, animated: , completion: ) 展示格式)
    ///
    /// - Parameters:
    ///   - presented: UIViewController
    ///   - flag: animated flag
    ///   - completion: completion
    public func present(to presented: UIViewController, animated flag: Bool, completion: Completion? = nil) -> Void {
        (self.navigationController ?? self).present(presented, animated: flag, completion: completion)
    }
}
