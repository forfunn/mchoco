//
//  MMach.swift
//  MChoco
//
//  Created by IT on 30/01/2019.
//  Copyright © 2019 None. All rights reserved.
//

import Foundation


/// 计算平分的距离
///
/// - Parameters:
///   - c: 距离
///   - number: 数量
///   - edge: 边界的宽度
/// - Returns: 计算后的结果
public func calc(for c: CGFloat, count number: UInt8, space edge: CGFloat) -> CGFloat {
    return (c - CGFloat(number + 1) * edge) / CGFloat(number)
}
