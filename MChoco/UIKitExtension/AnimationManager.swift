//
//  AnimationManager.swift
//  MChoco
//
//  Created by Richie on 2019/1/4.
//  Copyright © 2019 person. All rights reserved.
//

import UIKit

/// 动画管理类
public class AnimationManager: NSObject {
    /// 单例类
    public static let sharedManager = AnimationManager()
    
    /// 动画 时间间隔 0.3
    public static let duration: TimeInterval = 0.3
    
    /// percentComplete 进度
    public static let percentComplete: CGFloat = 0.5
    
    /// UIPercentDrivenInteractiveTransition 手势进度
    private var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition? = nil
    
}

// MARK: - method
extension AnimationManager {
    /// 开始
    public static func begin() -> Void {
        sharedManager.percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
    }
    
    /// 更新进度
    ///
    /// - Parameter progress: 进度 0.0 - 1.0
    public static func update(with progress: CGFloat) -> Void {
        guard let percentDrivenInteractiveTransition = sharedManager.percentDrivenInteractiveTransition else { return }
        percentDrivenInteractiveTransition.update(progress)
    }
    
    /// 决定是否是完成还是取消
    ///
    /// - Parameter percentComplete: 完成的系数 默认AnimationManager.percentComplete 当小于AnimationManager.percentComplete时 为取消操作
    /// 可以自行设置这个参数 0 - 1.0 之间
    public static func commit(_ percentComplete: CGFloat = AnimationManager.percentComplete) -> Void {
        guard let percentDrivenInteractiveTransition = sharedManager.percentDrivenInteractiveTransition else { return }
        defer {
            sharedManager.percentDrivenInteractiveTransition = nil
        }
        let progress = min(max(0, percentComplete), 1)
        percentDrivenInteractiveTransition.percentComplete >= progress ? percentDrivenInteractiveTransition.finish() : percentDrivenInteractiveTransition.cancel()
    }
}


// MARK: - AnimationManager : ModalStyle, ModalAnimation
extension AnimationManager {
    /// Modal
    ///
    /// - present: 弹出
    /// - dismiss: 消失
    fileprivate enum ModalStyle {
        case present
        case dismiss
    }
    
    /// Modal Animation
    fileprivate class ModalAnimation: NSObject {
        
        /// 类型
        private let style: ModalStyle
        
        /// 初始化方法
        ///
        /// - Parameter style: 类型
        init(with style:ModalStyle) {
            self.style = style
        }
    }
}


// MARK: - UIViewControllerTransitioningDelegate
extension AnimationManager: UIViewControllerTransitioningDelegate {
    /// present
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return AnimationManager.ModalAnimation(with: .present)
    }
    
    /// dismiss
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return AnimationManager.ModalAnimation(with: .dismiss)
    }
    
    /// dimiss animation
    ///
    /// - Parameter animator: UIViewControllerAnimatedTransitioning
    /// - Returns: UIViewControllerInteractiveTransitioning
    public func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        if let _ = animator as? AnimationManager.ModalAnimation {
            return self.percentDrivenInteractiveTransition
        }
        return nil
    }
}


// MARK: - UIViewControllerAnimatedTransitioning
extension AnimationManager.ModalAnimation: UIViewControllerAnimatedTransitioning {
    
    ///
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return AnimationManager.duration
    }
    
    ///
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromView = transitionContext.view(forKey: .from), let toView = transitionContext.view(forKey: .to), let to = transitionContext.viewController(forKey: .to), let from = transitionContext.viewController(forKey: .from) else {
            return
        }
        let containerView = transitionContext.containerView
        let finalFrame: CGRect = transitionContext.finalFrame(for: to)
        containerView.addSubview(toView)
        switch self.style {
        case .present:
            toView.frame = finalFrame.offsetBy(dx: finalFrame.width, dy: 0)
            UIView.animate(withDuration: self.transitionDuration(using: transitionContext), animations: {
                to.view.frame = finalFrame
            }) { (completeion) in
                transitionContext.completeTransition(completeion)
            }
        default:
            containerView.addSubview(fromView)
            UIView.animate(withDuration: self.transitionDuration(using: transitionContext), animations: {
                from.view.frame = finalFrame.offsetBy(dx: finalFrame.width, dy: 0)
            }) { (completeion) in
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            }
        }
    }
}
