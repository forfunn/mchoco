//
//  Typealias.swift
//  MChoco
//
//  Created by IT on 12/01/2019.
//  Copyright © 2019 None. All rights reserved.
//

import UIKit


/// Completion
public typealias Completion = () -> Void

/// 重命名 UIColor.ColorValue 存放颜色值
public typealias ColorValue = UIColor.ColorValue

/// 重命名 UInt
public typealias Color = UInt
