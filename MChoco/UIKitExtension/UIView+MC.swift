//
//  UIView+MC.swift
//  MChoco
//
//  Created by IT on 11/01/2019.
//  Copyright © 2019 None. All rights reserved.
//

import Foundation

// MARK: - UIView : Extension
extension UIView {
    
    /// 添加圆角
    ///
    /// - Parameters:
    ///   - radius: 圆角大小
    ///   - masksToBounds: 是否遮罩 默认true
    public func cornerRadius(with radius: CGFloat, masksToBounds: Bool = true) -> Void {
        layer.cornerRadius = radius
        layer.masksToBounds = masksToBounds
    }
    
    /// 增加点击手势
    ///
    /// - Parameters:
    ///   - target: 响应
    ///   - action: 响应的方法 @objc
    ///   - tapsRequired: 点击次数 默认1
    ///   - touchesRequired: 点击多少个手指 默认1
    /// - Returns: 手势
    public func addTapGestureRecognizer(with target: Any? = nil, action: Selector, tapsRequired: Int = 1, touchesRequired: Int = 1) -> UITapGestureRecognizer {
        let tapGestureRecognizer = UITapGestureRecognizer(target: target, action: action).configuration {
            $0.numberOfTapsRequired = tapsRequired
            $0.numberOfTouchesRequired = touchesRequired
        }
        
        if !isUserInteractionEnabled {
            isUserInteractionEnabled = true
        }
        addGestureRecognizer(tapGestureRecognizer)
        return tapGestureRecognizer
    }
    
    
    /// 截屏
    ///
    /// - Parameter scale: 缩放比例 请设置为屏幕的比例
    /// - Returns: 获取的图片
    public func snapshot(with scale: CGFloat = UIScreen.main.scale) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, scale)
        drawHierarchy(in: bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        defer {
            UIGraphicsEndImageContext()
        }
        return image
    }
}
