//
//  MoneyStyle.swift
//  MChoco
//
//  Created by Richie on 2019/1/6.
//  Copyright © 2019 person. All rights reserved.
//

import Foundation

/// 金额的格式
///
/// - int: Int 0
/// - float: Float 0.00
/// - custom: (format, empty) 格式 和 默认值
public enum MoneyStyle {
    /// 整数
    case int
    
    /// 浮点
    case float
    
    /// 自定义
    case custom(String, String)
    
    /// 默认值
    public var empty: String {
        get {
            switch self {
            case .int:
                return MoneyStyle.emptyInt
            case .float:
                return MoneyStyle.emptyFloat
            case .custom(_ , let empty):
                return empty
            }
        }
    }

    /// 金额的格式
    public var positiveFormat: String {
        get {
            switch self {
            case .int:
                return MoneyStyle.positiveFormatInt
            case .float:
                return MoneyStyle.positiveFormatFloat
            case .custom(let format, _):
                return format
            }
        }
    }

    /// Int 默认
    private static let emptyInt = "0"

    /// float 默认
    private static let emptyFloat = "0.00"

    /// Int 格式
    private static let positiveFormatInt = "#,##0"

    /// float 格式
    private static let positiveFormatFloat = "#,##0.##"
    
}
