//
//  UIView+SnapKit.swift
//  MChoco
//
//  Created by Richie on 2019/1/6.
//  Copyright © 2019 person. All rights reserved.
//

import UIKit
import SnapKit

// MARK: - UIView SnapKit Extension
extension UIView {
    
    /// safeAreaLayoutGuide left
    public var safeAreaLeft: ConstraintItem {
        if #available(iOS 11.0, *) {
            return self.safeAreaLayoutGuide.snp.left
        } else {
            return self.snp.left
        }
    }
    
    /// safeAreaLayoutGuide top
    public var safeAreaTop: ConstraintItem {
        if #available(iOS 11.0, *) {
            return self.safeAreaLayoutGuide.snp.top
        } else {
            return self.snp.top
        }
    }
    
    /// safeAreaLayoutGuide right
    public var safeAreaRight: ConstraintItem {
        if #available(iOS 11.0, *) {
            return self.safeAreaLayoutGuide.snp.right
        } else {
            return self.snp.right
        }
    }
    
    /// safeAreaLayoutGuide bottom
    public var safeAreaBottom: ConstraintItem {
        if #available(iOS 11.0, *) {
            return self.safeAreaLayoutGuide.snp.bottom
        } else {
            return self.snp.bottom
        }
    }
    
    /// safeAreaLayoutGuide leading
    public var safeAreaLeading: ConstraintItem {
        if #available(iOS 11.0, *) {
            return self.safeAreaLayoutGuide.snp.leading
        } else {
            return self.snp.leading
        }
    }
    
    /// safeAreaLayoutGuide trailing
    public var safeAreaTrailing: ConstraintItem {
        if #available(iOS 11.0, *) {
            return self.safeAreaLayoutGuide.snp.trailing
        } else {
            return self.snp.trailing
        }
    }
    
    /// safeAreaLayoutGuide edges
    public var safeAreaEdges: ConstraintItem {
        if #available(iOS 11.0, *) {
            return self.safeAreaLayoutGuide.snp.edges
        } else {
            return self.snp.edges
        }
    }
}

