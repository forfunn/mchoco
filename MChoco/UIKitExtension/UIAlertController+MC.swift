//
//  UIAlertController+MC.swift
//  MChoco
//
//  Created by IT on 11/01/2019.
//  Copyright © 2019 None. All rights reserved.
//

import Foundation

// MARK: - UIAlertController
extension UIAlertController {
    /// 初始化方法
    ///
    /// - Parameters:
    ///   - title: 标题
    ///   - message: 信息
    ///   - ok: 确定
    ///   - cancel: 取消
    convenience init(with title: String? = nil, message: String? = nil, ok: UIAlertAction? = nil, cancel: UIAlertAction? = nil) {
        self.init(title: title, message: message, preferredStyle: .alert)
        if let ok = ok {
            self.addAction(ok)
        }
        if let cancel = cancel {
            self.addAction(cancel)
        }
        if self.actions.count == 0 {
            self.addAction(UIAlertAction(title: "确定", style: .cancel, handler: nil))
        }
    }
    
    
    /// actionSheet
    ///
    /// - Parameters:
    ///   - title: 标题
    ///   - message: message
    ///   - actionSheets: 点击操作
    convenience init(with title: String? = nil, message: String? = nil, actionSheets: [UIAlertAction]) {
        self.init(title: title, message: message, preferredStyle: .actionSheet)
        actions.forEach {
            self.addAction($0)
        }
    }
    
    /// 提示消息
    ///
    /// - Parameters:
    ///   - title: 标题
    ///   - message: message
    ///   - okClouse: 点击操作
    convenience init(_ title: String? = nil, message: String? = nil, okClouse: (title: String, action: (() -> (Void))?) = ("取消", nil)) {
        self.init(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: okClouse.title, style: .cancel) { (action) in
            if let actions = okClouse.action {
                actions()
            }
        }
        self.addAction(action)
    }
    
    
}
