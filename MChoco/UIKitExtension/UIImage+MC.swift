//
//  UIImage+MC.swift
//  MChoco
//
//  Created by IT on 11/01/2019.
//  Copyright © 2019 None. All rights reserved.
//

import Foundation


// MARK: - UIImage 扩展
extension UIImage {
    
    /// 根据纯色生成图片
    ///
    /// - Parameters:
    ///   - color: 颜色值
    ///   - size: 大小
    /// - Returns: 成功为image 不成功为nil
    public static func pure(with color: UIColor, size: CGSize = CGSize(width: 1.0, height: 1.0)) -> UIImage {
        UIGraphicsBeginImageContext(size)
        defer {
            UIGraphicsEndImageContext()
        }
        
        if let context = UIGraphicsGetCurrentContext() {
            context.setFillColor(color.cgColor)
            context.fill(CGRect(origin: .zero, size: size))
            if let image = UIGraphicsGetImageFromCurrentImageContext() {
                return image
            }
        }
        return UIImage()
    }
    
    /// 获取圆角图片
    ///
    /// - Parameter radius: 圆角
    /// - Returns: 圆角图片
    public func image(with radius: CGFloat) -> UIImage {
        if radius <= 0 {
            return self
        }
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        defer {
            UIGraphicsEndImageContext()
        }
        let frame = CGRect(origin: .zero, size: size)
        UIBezierPath(roundedRect: frame, cornerRadius: radius).addClip()
        draw(in: frame)
        return UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
    }
}
