//
//  UINavigationController+MC.swift
//  MChoco
//
//  Created by IT on 12/01/2019.
//  Copyright © 2019 None. All rights reserved.
//

import Foundation

import CoreGraphics

// MARK: - UINavigationController method
extension UINavigationController {
    
    /// push viewController
    ///
    /// - Parameters:
    ///   - viewController: viewController
    ///   - animated: animated
    ///   - completion: completion when animated you want to do
    public func push(_ viewController: UIViewController, animated: Bool, completion: @escaping Completion) -> Void {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        defer {
            CATransaction.commit()
        }
        self.pushViewController(viewController, animated: animated)
    }
    
    /// pop
    ///
    /// - Parameters:
    ///   - animated: animated
    ///   - completion: completion when animated you want to do
    /// - Returns: Returns the popped controller.
    public func pop(_ animated: Bool, completion: @escaping Completion) -> UIViewController? {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        defer {
            CATransaction.commit()
        }
        return self.popViewController(animated: animated)
    }
    
    /// pop to viewController which existed in ViewControllers
    ///
    /// - Parameters:
    ///   - viewController: viewController
    ///   - animated: viewController
    ///   - completion: completion when animated you want to do
    /// - Returns: Returns the popped controllers.
    public func pop(to viewController: UIViewController, animated: Bool, completion: @escaping Completion) -> [UIViewController]? {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        defer {
            CATransaction.commit()
        }
        return self.popToViewController(viewController, animated: animated)
    }
    
    /// pop to rootViewController
    ///
    /// - Parameters:
    ///   - animated: animated
    ///   - completion: completion when animated you want to do
    /// - Returns: Returns the popped controllers.
    public func pop(toRoot animated: Bool, completion: @escaping Completion) -> [UIViewController]? {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        defer {
            CATransaction.commit()
        }
        return self.popToRootViewController(animated: animated)
    }
}
