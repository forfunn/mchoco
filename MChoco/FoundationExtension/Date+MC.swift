//
//  Date+MC.swift
//  MChoco
//
//  Created by Richie on 2019/1/6.
//  Copyright © 2019 person. All rights reserved.
//

import Foundation

// MARK: - Date Extension
extension Date {
    
    /// 日期字符串
    ///
    /// - Parameter style: 格式
    /// - Returns: 日期对应的字符串
    public func string(with style: DateStyle) -> String {
        return style.formatter.string(from: self)
    }
}


// MARK: - String Date Extension
extension String {
    
    /// 读取字符串中的时间格式
    ///
    /// - Parameter style: 格式
    /// - Returns: 日期
    public func date(with style:DateStyle) -> Date? {
        return style.formatter.date(from: self)
    }
}
