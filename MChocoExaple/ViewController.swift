//
//  ViewController.swift
//  MChocoExaple
//
//  Created by IT on 14/01/2019.
//  Copyright © 2019 None. All rights reserved.
//

import UIKit

import MChoco

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.view.backgroundColor = UIColor.white
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapGesture)))
        
        let gesture = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(edgePanGestureRecognizer(_ :)))
        gesture.edges = .left
//        gesture.direction = .right
        self.view.addGestureRecognizer(gesture)
        
        gesture.delegate = self
    }

    
    @objc func tapGesture() -> Void {
        let c = ViewController()
        if let title = self.title, !title.isEmpty {
            self.navigationController?.pushViewController(c, animated: true)
        } else {
            let controller = UINavigationController(rootViewController: c)
            controller.transitioningDelegate = AnimationManager.sharedManager
            c.title = "1111"
            self.present(to: controller, animated: true)
        }
    }
    
    
    @objc func edgePanGestureRecognizer(_ gestrue: UIScreenEdgePanGestureRecognizer) -> Void {
        let translation = gestrue.translation(in: gestrue.view)
        switch gestrue.state {
        case .began:
            AnimationManager.begin()
            self.dismiss(animated: true, completion: nil)
        case .changed:
            let progress = translation.x / (gestrue.view)!.bounds.width
//            if AnimationManager
            AnimationManager.update(with: min(max(progress, 0), 1))
        case .cancelled, .ended:
            AnimationManager.commit()
        default:
            break
            
        }
    }

}

extension ViewController: UIGestureRecognizerDelegate {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if let navigationController = self.navigationController {
            if navigationController.viewControllers.count == 1 {
                if gestureRecognizer.isKind(of: UIScreenEdgePanGestureRecognizer.self) {
                    return true
                }
            } else {
                
            }
        }
        return true
    }
}

